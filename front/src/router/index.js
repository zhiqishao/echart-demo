import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import echart1 from '../views/modules/echarts/echart1'
import echart2 from '../views/modules/echarts/echart2'
import echart3 from '../views/modules/echarts/echart3'
import echart4 from '../views/modules/echarts/echart4'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/echart1',
      name: 'echart1',
      component: echart1
    },
    {
      path: '/echart2',
      name: 'echart2',
      component: echart2
    },
    {
      path: '/echart3',
      name: 'echart3',
      component: echart3
    },
    {
      path: '/echart4',
      name: 'echart4',
      component: echart4
    }

  ]
})

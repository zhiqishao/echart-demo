package com.example.demo.controller;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: shaozhiqi
 * @Date: 2022/2/28
 */
@Slf4j
@RestController
@RequestMapping("/echart")
public class EchartController {

    @GetMapping("/echart3")
    public Object getData3() {
        Map returnMap = new HashMap<>();
        returnMap.put("code", 1);
        List<Map> dataList = new ArrayList<>();
        returnMap.put("dataList", dataList);
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j < 4; j++) {
                Map map = new HashMap();
                map.put("key", i + "_" + j);
                map.put("type", new Random().nextInt(3)+1);
                map.put("are", j);
                map.put("title", "title" + "_" + i + "_" + j);
                List<Map> subList = new ArrayList<>();
                for (int k = 0; k < 5; k++) {
                    Map subMap = new HashMap();
                    subMap.put("name", "name" + i + "_" + j + "_" + k);
                    subMap.put("value", new Random().nextInt(1000));
                    subList.add(subMap);
                }
                map.put("subList", subList);
                dataList.add(map);
            }
        }
        //log.info("result:" + JSON.toJSON(returnMap));
        return returnMap;
    }
}

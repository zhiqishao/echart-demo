package com.example.demo.test001;

import com.example.demo.utils.RsaEncrypt;
import org.junit.jupiter.api.Test;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.*;

public class Test1 {

    @Test
    public void test() {
        Map<Long, String> idsMap1 = new HashMap<>();
        idsMap1.put(0L, "0");
        idsMap1.put(1L, "1");
        idsMap1.put(2L, "2");
        idsMap1.put(3L, "3");
        Set<Long> ids1 = new HashSet<>(idsMap1.keySet());
        List<Long> ids2 = new ArrayList<>(Arrays.asList(new Long[]{2L, 3L, 4L}));
        ids1.removeAll(ids2);
        System.out.println(ids1);
        ids2.removeAll(idsMap1.keySet());
        System.out.println(ids2);
        System.out.println(ids1.toArray(new Long[0]).length);
    }

    @Test
    public void testBreak() {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            if (i == 7) {
                break;
            }
        }
    }

    @Test
    public void testEquals() {
        String str1 = new String("");// 非new 如下两个判断都成立，new只有equals成立
        if (str1 == "") {
            System.out.println("==");
        }

        if ("".equals(str1)) {
            System.out.println("equals");
        }
    }

    @Test
    public void testForeach() {
        Map<String, String> testMap = new HashMap<>();
        testMap.put("", "1");
        testMap.put("2", "dfd");
        testMap.put("3", "gdfg");
        testMap.forEach((key, v) -> {
            if (key == null || "".equals(key)) {
                return;
            }
            System.out.println(key);
        });
    }

    @Test
    public void testMap() {
        Map<String, String> paramMap = new HashMap<>();
        if (paramMap.isEmpty()) {
            System.out.println("dfdfd");
        }
        System.out.println(paramMap.get("11"));
    }


    @Test
    public void testThread(){
        ExecutorService execute=new ThreadPoolExecutor(1,2,1000, TimeUnit.MILLISECONDS,new SynchronousQueue<Runnable>(), Executors.defaultThreadFactory(),new ThreadPoolExecutor.AbortPolicy());
        execute.execute(new Thread("ddd"));
        execute.submit(new Thread("ddd"));
    }

    @Test
    public void testA(){
        String [] ar={"121","dd"};
        List<String> list=Arrays.asList(ar);
        list.add("dd");// error java.lang.UnsupportedOperationException
    }

    @Test
    public void testHashMap(){
        Map map=new HashMap();
        map.put("1",null);
        System.out.println(map.get("1"));
    }

    @Test
    public void testSub(){
        String s="222+444+";
        System.out.println(s.split("\\+").length);
    }

    @Test
    public void testDivice(){
        System.out.println(0/1);
    }

    @Test
    public void testCalendar(){
        Calendar now=new GregorianCalendar();
        int i = now.get(Calendar.MONTH);
        System.out.println(i);
    }

    /**
     * capEsPublicKey: MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCJ8OblKZbPmohL//blw5l29FbbQ+gQ8vs+SZr694eA9IjdZ3BHddIGLtwKqzffEYORaFOtFhLN6XrVRxkzige5bLZupbMlcJJcr5zWDVthSJB2geJiSXR6t/9C8ufKHuN8Hv/86zOhEM5vddOvIGsW7q7XmpC2jQC1D/JtYk5hgwIDAQAB
     * capEsPrivateKey: MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAInw5uUpls+aiEv/9uXDmXb0VttD6BDy+z5Jmvr3h4D0iN1ncEd10gYu3AqrN98Rg5FoU60WEs3petVHGTOKB7lstm6lsyVwklyvnNYNW2FIkHaB4mJJdHq3/0Ly58oe43we//zrM6EQzm91068gaxburteakLaNALUP8m1iTmGDAgMBAAECgYB1U6kg3BoBZOBvPD0FuzRKIJPMFltKJ1IZ7cNbmbTws43CPBwdPBKy3aKdlHqGSsBMqp8Zhr/h0qh+xeWil9Huz4wDSIEGWCN76IcD6CjTq8SIHgLtA/aRvgqdo/HSrEp7ZM+yXavtrvZQTo0+JAv3BVwdn0WcWHZIqL7OllntKQJBAP/ZPbdurzLM5A7X1TPRH/V34LLzxrzvIbyfzCkTWdTLgWlWg/YE1KTsxwkM3/L3hQMQ1XOFv0NzZDk4WCOwj4cCQQCKBcyBCbJnAPB4KIs69kts5VL9TpYUaj51GHl7S76WJkEY+9MQyqXsRi6Ydms9HITNfWD1WdIvIzuikn+oJAUlAkBVy6rXjWVw+qZrl9MmGjXGYhnWyMrXZrWAhHG5QIT6R/gws9Nyt7H7UG96SHf9CSXwnj+GCs7VtqdgTNW5uiaXAkAH3Si3uWu2vrD0x6qXI21MuQwvbojShPMmDMzvH3OubmOLHt35uPIXMjB5c1XM15EhfCFIGWQGV4WQT7SCSfsZAkEAuNKGyNQHHKe7JT8FF7mj6QLlvEu6zBUD0UKCi65UCXirAb48BJTdfg051LL/qpDc4TVzLrnQhnJow7vWLijTbg==
     *
     */
    @Test
    public void testEntro(){
        String capEsPublicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYKIagm0vuh0vNF5ZbKLN20Ztyl8jyPOQI2fKi2Lw+GPkEGHYyx2kDTgBkyl6yDEmp3+W7PU1XuiU834m/V5nNDSGJiXIHv/s6PRnydsdxKE0c77tYcT0vnN/rkfOlQONb09iRz6dI/+PCk1ByTknXp7UYIS3x6DaQmM9T+cLE9wIDAQAB";
        // admin admin-123
        String userInfo="supertong"+","+"Eyccass123!";
        try {
            String encrypt = RsaEncrypt.encrypt(userInfo, capEsPublicKey);
            System.out.println(encrypt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEcc() throws Exception {
       // uat String capEsPublicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCJ8OblKZbPmohL//blw5l29FbbQ+gQ8vs+SZr694eA9IjdZ3BHddIGLtwKqzffEYORaFOtFhLN6XrVRxkzige5bLZupbMlcJJcr5zWDVthSJB2geJiSXR6t/9C8ufKHuN8Hv/86zOhEM5vddOvIGsW7q7XmpC2jQC1D/JtYk5hgwIDAQAB";
       // String capEsPrivateKey="MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAInw5uUpls+aiEv/9uXDmXb0VttD6BDy+z5Jmvr3h4D0iN1ncEd10gYu3AqrN98Rg5FoU60WEs3petVHGTOKB7lstm6lsyVwklyvnNYNW2FIkHaB4mJJdHq3/0Ly58oe43we//zrM6EQzm91068gaxburteakLaNALUP8m1iTmGDAgMBAAECgYB1U6kg3BoBZOBvPD0FuzRKIJPMFltKJ1IZ7cNbmbTws43CPBwdPBKy3aKdlHqGSsBMqp8Zhr/h0qh+xeWil9Huz4wDSIEGWCN76IcD6CjTq8SIHgLtA/aRvgqdo/HSrEp7ZM+yXavtrvZQTo0+JAv3BVwdn0WcWHZIqL7OllntKQJBAP/ZPbdurzLM5A7X1TPRH/V34LLzxrzvIbyfzCkTWdTLgWlWg/YE1KTsxwkM3/L3hQMQ1XOFv0NzZDk4WCOwj4cCQQCKBcyBCbJnAPB4KIs69kts5VL9TpYUaj51GHl7S76WJkEY+9MQyqXsRi6Ydms9HITNfWD1WdIvIzuikn+oJAUlAkBVy6rXjWVw+qZrl9MmGjXGYhnWyMrXZrWAhHG5QIT6R/gws9Nyt7H7UG96SHf9CSXwnj+GCs7VtqdgTNW5uiaXAkAH3Si3uWu2vrD0x6qXI21MuQwvbojShPMmDMzvH3OubmOLHt35uPIXMjB5c1XM15EhfCFIGWQGV4WQT7SCSfsZAkEAuNKGyNQHHKe7JT8FF7mj6QLlvEu6zBUD0UKCi65UCXirAb48BJTdfg051LL/qpDc4TVzLrnQhnJow7vWLijTbg==";
        String capEsPublicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYKIagm0vuh0vNF5ZbKLN20Ztyl8jyPOQI2fKi2Lw+GPkEGHYyx2kDTgBkyl6yDEmp3+W7PU1XuiU834m/V5nNDSGJiXIHv/s6PRnydsdxKE0c77tYcT0vnN/rkfOlQONb09iRz6dI/+PCk1ByTknXp7UYIS3x6DaQmM9T+cLE9wIDAQAB";
        /* // admin admin-123
        String userInfo=
        //生成公钥和私钥
        RsaEncrypt.genKeyPair();*/
        //加密字符串 	supertong Eyccass123!
        // vecs
        //vecsQWE123!
        String message = "vecs"+","+"vecsQWE123!";

        String messageEn = RsaEncrypt.encrypt(message, capEsPublicKey);
        //String encodeM = URLEncoder.encode(messageEn, "UTF-8");
        System.out.println(message + "\t加密后的字符串为:" + messageEn);
        //String decodeD = URLDecoder.decode(encodeM, "UTF-8");
       /* String messageDe = RsaEncrypt.decrypt(messageEn, capEsPrivateKey);
        System.out.println("还原后的字符串为:" + messageDe);*/
    }

    @Test
    public void testGetRsa() throws Exception{
        Map<Integer, String> integerStringMap = RsaEncrypt.genKeyPair();
        // 0表示公钥，1表示私钥
        // pub::MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYKIagm0vuh0vNF5ZbKLN20Ztyl8jyPOQI2fKi2Lw+GPkEGHYyx2kDTgBkyl6yDEmp3+W7PU1XuiU834m/V5nNDSGJiXIHv/s6PRnydsdxKE0c77tYcT0vnN/rkfOlQONb09iRz6dI/+PCk1ByTknXp7UYIS3x6DaQmM9T+cLE9wIDAQAB
        // pri::MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJgohqCbS+6HS80Xllsos3bRm3KXyPI85AjZ8qLYvD4Y+QQYdjLHaQNOAGTKXrIMSanf5bs9TVe6JTzfib9Xmc0NIYmJcge/+zo9GfJ2x3EoTRzvu1hxPS+c3+uR86VA41vT2JHPp0j/48KTUHJOSdentRghLfHoNpCYz1P5wsT3AgMBAAECgYA8gGi/teBkcSG/5DAiATYS+deQojABuhqGNntXK0z2eCGT8lIVcNsduEdJzHoI2//ybxe+8uotBtRFtzDjh2ac1ks1hpK+wPXIZL5WGFk2ZUVZ1lJKT+7A/PZumdzOmznUnYAqRHnHGhx/26nNlsElnUsH9zDp1fCKVQ5rfKHliQJBAOa6ErkRSn8lwdW86cj0iOnfbn9wmY9ViBkhH99lf17noqJZltU1fncOa2M8D7cxg3WyoyW173w6iASaq+whwLMCQQCo00Xfpr2rM4zK/WZzawCzVbrR5o5AjB3L9NTKnqW/8vtTBupZm5iIOmsoerK6UPCvZSASjllBoVE699nrLEStAkEA5oazfkVQcYmACdYen7HzufiLprkSNKv4t2LpPYNjp6c4T43DrnMsUDgQA65W5WqLNmWtrFgDV9QDYtZHQD7ZEQJBAJJO5pM4kc840Z9pk8waw3Snq0wVHtrZk0kaWP9wFf0Purbtm1MlAT+uQQfHrfTCCJQ4c18hdkDmaNtNyNGCf9ECQATtX0Ux5oytpgyRilIEjTILWgXUIrNc11HMXzmfGXQ7lqOFGcJTXZrzc5GqNbxRhkgfDckhvSTpywVzEgt9skc=
        integerStringMap.forEach((k,v)->{
            System.out.println(k);
            System.out.println(v);
        });
    }
}
